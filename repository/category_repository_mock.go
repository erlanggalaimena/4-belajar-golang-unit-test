package repository

import (
	"belajar-golang-unit-test/entity"

	"github.com/stretchr/testify/mock"
)

type CategoryRepositoryMock struct {
	Mock mock.Mock
}

func (repository *CategoryRepositoryMock) FindById(id int) *entity.Category {
	var arguments mock.Arguments = repository.Mock.Called(id)

	if arguments.Get(0) == nil {
		return nil
	}

	var category entity.Category = arguments.Get(0).(entity.Category)

	return &category
}
