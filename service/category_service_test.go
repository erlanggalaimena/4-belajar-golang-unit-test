package service

import (
	"belajar-golang-unit-test/entity"
	"belajar-golang-unit-test/repository"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var categoryRepository *repository.CategoryRepositoryMock = &repository.CategoryRepositoryMock{Mock: mock.Mock{}}
var categoryService CategoryService = CategoryService{Repository: categoryRepository}

func TestCategoryServiceGet(t *testing.T) {
	// * Pada Bagian ini, kita sebetulnya membuat memprogram apabila kita memasukan 1 ke method interface FindById, maka balikannya NIL
	// * Tidak selesai sampai deklarasi disini saja
	// * Kita memerlukan repository yang mengimplementasikan Mock Struct
	// * Karena diatas sudah diimplementasikan CategoryRepositoryMock yang merupakan turunan dari CategoryRepository dan dimasukan ke struct CategoryService
	// * Maka ketika kita memanggil fungsi FindById berarti kita memanggil fungsi dari struct CategoryRepositoryMock dan CategoryRepositoryMock mengimplementasikan Mock yang akhirnya Command dibawah bisa berfungsi sebagaimana mestinya
	// * Apabila line code dibawah di command, otomatis Mock yang dimiliki variabel categoryRepository masih kosong.
	// * Nantinya ketika dijalankan FindById akan menghasilkan error, karena dia Mock Struct yang ada pada struct CategoryRepositoryMock tidak tau harus kembalikan nilai apa
	// * Begitupun kalau kita isi Idnya dengan angka selain 1
	categoryRepository.Mock.On("FindById", 1).Return(nil)
	categoryRepository.Mock.On("FindById", 2).Return(entity.Category{Id: 2, Name: "Handphone"})

	category, err := categoryService.FindById(1)

	assert.Nil(t, category)
	assert.NotNil(t, err)

	category, err = categoryService.FindById(2)

	assert.NotNil(t, category)
	assert.Nil(t, err)
	assert.Equal(t, 2, category.Id)
	assert.Equal(t, "Handphone", category.Name)
}
