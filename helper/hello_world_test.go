package helper

import (
	"fmt"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

/*
* TestMain merupakan fungsi untuk mengatur eksekusi unit test dalam sebuah package
* Go akan secara otomatis menjalankan fungsi TestMain ini setiap kali menjalankan perintah unit test terhadap package ini
* Hal diatas bisa tercapai hanya jika deklarasi fungsinya sesuai kontrak / aturan dari golang yaitu nama fungsi harus TestMain dan menerima 1 parameter tipe pointer struct M
 */
func TestMain(t *testing.M) {
	// * Before
	fmt.Println("BEFORE UNIT TESTING HELPER PACKAGE EXECUTION")

	t.Run()

	// * After
	fmt.Println("AFTER UNIT TESTING HELPER PACKAGE EXECUTION")
}

func TestHelloWorld(t *testing.T) {
	/*
	* Go memiliki beberapa aturan yang harus dipenuhi apabila ingin sebuah file go diidentifikasi sebagai unit test
	* Yang pertama adalah penamaan filenya. Penamaan file untuk unit test harus diakhiri dengan tulisan "_test". Contohnya adalah "hello_world_test.go"
	* Nama fungsi yang digunakan untuk implementasi Unit Test harus diawali dengan kata Test. Contohnya TestHelloWorld(). Tidak ada aturan kalimat belakangnya harus apa jadi bebas-bebas saja mau kalimat yang seperti apa setelah tulisan "Test"
	* Fungsi tidak boleh memiliki return value
	* Parameter fungsi harus memiliki tipe struct *testing.T
	* *testing.T merupakan tipe data struct dari package testing yang digunakan untuk melakukan Unit Test
	* Untuk menjalankan Unit Test menggunakan command go test
	* Sebelum menjalankan command go test, kita harus masuk dulu ke folder dimana file unit test sudah didefinisikan
	* go test otomatis akan menjalankan semua file unit test beserta semua fungsinya
	* Apabila kita ingin melihat detail apa saja fungsi yang dijalankan selama unit test berlangsung, gunakan command go test -v
	* Apabila kita ingin spesifik menjalankan fungsi unit test dengan pattern nama tertentu, gunakan command go test -v -run=NamaFungsi
	* Kita bisa menjalankan proses Unit Test pada level root. Caranya adalah dengan menambahkan tulisan "./..." setelah go test
	* go test ./... akan menjalankan semua package yang ada Unit Test
	 */
	result := HelloWorld("Angga")

	if result != "Hello Angga" {
		/*
		* Fail merupakan fungsi dari package testing yang berfungsi untuk membuat error unit testing.
		* Cara kerjanya ketika fungsi Fail dipanggil, Unit Test tidak akan langsung berhenti dan melanjutkan sisa codenya sampai selesai
		* Namun hasil akhir tetap akan dianggap fail oleh sistem
		 */
		// t.Fail()

		/*
		* Error berfungsi print pesan yang menyebabkan terjadinya error. Lalu fungsi Error akan memanggil fungsi Fail
		 */
		t.Error("String Must Hello Angga")
	}

	fmt.Println("TestHelloWorld Done")
}

func TestHelloWorldEko(t *testing.T) {
	result := HelloWorld("Eko")

	if result != "Hello Eko" {
		/*
		* FailNow merupakan fungsi dari package testing yang berfungsi untuk membuat error unit testing.
		* Cara kerjanya ketika fungsi FailNow dipanggil, Unit Test akan langsung berhenti dan tidak melanjutkan sisa codenya sampai selesai
		 */
		// t.FailNow()

		/*
		* Fatal berfungsi print pesan yang menyebabkan terjadinya error. Lalu fungsi Fatal akan memanggil fungsi FailNow
		 */
		t.Fatal("String Must Hello Eko")
	}

	fmt.Println("TestHelloWorldEko Done")
}

func TestHelloWorldWithAssert(t *testing.T) {
	var result string = HelloWorld("Harumichi")

	/*
	* Apabila ingin melakukan proses perbandingan nilai, sebaiknya menggunakan package assert
	* Package assert sendiri didapatkan dari library external dan bukan bawan dari go
	* Untuk librarynya sendiri menggunakan testify yang sudah populer dan bisa dilihat di https://github.com/stretchr/testify
	* Apabila terjadi sebuah error, method Equal pada package assert akan memanggil fungsi Fail milik Go
	 */
	assert.Equal(t, "Hello Harumichi", result)

	fmt.Println("TestHelloWorldWithAssert Done")
}

func TestHelloWorldWithRequire(t *testing.T) {
	var result string = HelloWorld("Bouya")

	/*
	* Package require memiliki fungsi yang sama seperti fungsi yang dimiliki oleh package assert
	* Perbedaan diantara package assert dan require adalah penanganan ketika memanggil error
	* Apabila terjadi sebuah error, method Equal pada package require akan memanggil fungsi FailNow milik Go
	 */
	require.Equal(t, "Hello Harumichi", result)

	fmt.Println("TestHelloWorldWithRequire Done")
}

func TestSkip(t *testing.T) {
	// * GOOS merupakan atribut konstan dimiliki package runtime yang memiliki fungsi untuk menyimpan nama tipe OS
	if runtime.GOOS == "linux" {
		// * Skip merupakan salah satu fungsi dari struct T yang berfungsi untuk melakukan skip unit test
		t.Skip("Cannot Run On Linux OS")
	}

	result := HelloWorld("Ichigo")

	assert.Equal(t, "Hi Ichigo", result)
}

func TestSubTest(t *testing.T) {
	/*
	* Kita bisa melakukan sub test didalam Go
	* Sub Test maksudnya adalah didalam sebuah fungsi Test, kita melakukan sebuah Test lagi
	* Caranya yaitu pergunakan fungsi Run yang ada pada struct *testing.T
	 */
	t.Run("Erlangga", func(t *testing.T) {
		var result string = HelloWorld("Erlangga")

		require.Equal(t, "Hello Erlangga", result, "Result must be 'Hello Erlangga'")
	})

	t.Run("Laimena", func(t *testing.T) {
		var result string = HelloWorld("Laimena")

		require.Equal(t, "Hello Laimena", result, "Result must be 'Hello Laimena'")
	})
}

func TestTableTest(t *testing.T) {
	/*
	* Table Test Sebetulnya sebuah konsep untuk melakukan unit test
	* Konsepnya adalah kita memanfaatkan sebuah slice untuk melakukan pengetesan unit test yang sama dengan parameter yang berbeda secara berulang
	* Kalau sebelumnya contoh diatas, apabila kita misalnya ingin melakukan unit test terhadap fungsi HelloWorld dengan menggunakan nama Angga maka kita buat fungsi TestHelloWorld yang spesifik hanya untuk test masukan Angga terhadap fungsi HelloWorld
	* Kalau kita mau memasukan variabel lain seperti contohnya eko, maka kita antara membuat fungsi baru atau difungsi yang sama kita mendeklarasikan hal yang sama
	* Hal ini sangat membosankan apabila test casenya sangat banyak
	* dengan konsep Table Test, kita bisa mereduksi / mengurangi pengulangan dalam membuat code yang sama untuk melakukan unit test dan cukup melakukan pengulangan dalam membuat data test casenya
	 */
	testCase := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "Erlangga",
			request:  "Erlangga",
			expected: "Hello Erlangga",
		},
		{
			name:     "Bouya",
			request:  "Bouya",
			expected: "Hello Bouya",
		},
		{
			name:     "Ichigo",
			request:  "Ichigo",
			expected: "Hello Ichigo",
		},
		{
			name:     "Kazuya",
			request:  "Kazuya",
			expected: "Hi Ichigo",
		},
	}

	for _, test := range testCase {
		t.Run(test.name, func(t *testing.T) {
			result := HelloWorld(test.request)

			require.Equal(t, test.expected, result)
		})
	}
}

/*
* Benchmark adalah mekanisme menghitung performa dari sebuah kode
* Go sudah menyiapkan package yang bisa digunakan untuk melakukan Benchmark yaitu di package testing
* Dalam implementasinya, kita akan menggunakan struct B dari package testing yang sebenarnya merupakan singkatan dari Benchmark
* Beberapa aturan main yang ada Unit Test juga berlaku saat kita mendefinisikan sebuah Benchmark seperti:
* Nama fungsi Benchmark harus diawali dengan tulisan Benchmark
* fungsi tidak boleh memiliki return value
* fungsinya menerima 1 parameter yaitu struct B
* fungsinya dideklarasikan di sebuah file yang diperuntukan untuk unit testing (nama file belakangnya ada tulisan _test.go)
 */
func BenchmarkHelloWorld(b *testing.B) {
	// * Kita cukup mendeklarasikan iterasi sebanyak N dari struct B. Selanjutnya Go yang akan mengatur semua testingnya
	for i := 0; i < b.N; i++ {
		HelloWorld("Eko")
	}
}

func BenchmarkHelloWorldKurniawan(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Kurniawan")
	}
}

func BenchmarkSub(b *testing.B) {
	b.Run("Eko", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Eko")
		}
	})

	b.Run("Kurniawan", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Kurniawan")
		}
	})
}

func BenchmarkTable(b *testing.B) {
	testCase := []struct {
		name    string
		request string
	}{
		{
			name:    "Erlangga",
			request: "Erlangga",
		},
		{
			name:    "Laimena",
			request: "Laimena",
		},
		{
			name:    "Goku",
			request: "Goku",
		},
	}

	for _, test := range testCase {
		b.Run(test.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				HelloWorld(test.request)
			}
		})
	}
}
